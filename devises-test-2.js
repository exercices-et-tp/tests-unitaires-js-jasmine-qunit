function assert(message, expr) {
    if (!expr) {
        output(false, message);
        throw new Error(message);
    }
    output(true, message);
}

function output(result, message) {
    var p = document.createElement('p');
    message += result ? ' : SUCCESS' : ' : FAILURE';
    p.style.color = result ? '#0c0' : '#c00';
    p.innerHTML = message;
    document.body.appendChild(p);
}

function testCase(message, tests) {
    // Initialisations
    var total = 0;
    var succeed = 0;
    // Tests si initialisation et désactivation présentes
    var hasSetup = typeof tests.setUp === 'function';
    var hasTeardown = typeof tests.tearDown === 'function';
    var h = document.createElement('h3');
    h.innerHTML = message;
    document.body.appendChild(h);
    for (test in tests) {
        if (test !== 'setUp' && test !== 'tearDown') {
            total++;
        }
        try {
            if (hasSetup) {
                tests.setUp();
            }
            tests[test]();
            if (test !== 'setUp' && test !== 'tearDown') {
                succeed++;
            }
            if (hasTeardown) {
                tests.tearDown();
            }
        } catch (err) {
        }
    }
    var p = document.createElement('p');
    p.innerHTML = 'succeed tests ' + succeed + '/' + total;
    document.body.appendChild(p);
}

// Définition des cas
testCase('Conversion euro → dollars us (USD)', {
    'setUp': function () {
        this.currency = 'USD';
    },
    'Test avec 1 euro': function () {
        assert('1€ doit renvoyer 1,3$', convertirEuro(1, this.currency) === 1.3);
    },
    'Test avec 2 euro': function () {
        assert('2€ doit renvoyer 2,6$', convertirEuro(2, this.currency) === 2.6);
    }
})


testCase('Conversion euro → livres anglaises (GBP)', {
    'setUp': function () {
        this.currency = 'GBP';
    },
    'Test avec 1 euro': function () {
        assert('1€ doit renvoyer 0,87£', convertirEuro(1, this.currency) === 0.87);
    },
    'Test avec 2 euro': function () {
        assert('2€ doit renvoyer 1,74£', convertirEuro(2, this.currency) === 1.74);
    }
})


testCase('Conversion euro → Yen Japonais (JPY)', {
    'setUp': function () {
        this.currency = 'JPY';
    },
    'Test avec 1 euro': function () {
        assert('1€ doit renvoyer  124,77¥', convertirEuro(1, this.currency) === 124.77);
    },
    'Test avec 2 euro': function () {
        assert('2€ doit renvoyer  249.54¥', Math.abs(convertirEuro(2, this.currency) -249.54)<=tolerance);
    }
})

testCase('Conversion euro → devise non gérée, CRN', {
    'Test devise CRN ': function () {
        var messsage;
        try{
         convertirEuro(1, 'CRN')
        } catch(err){
         message = err.message;
        }
        assert('La conversion euro → crn doit renvoyer une erreur', message === 'Monnaie non geree');    }
})

