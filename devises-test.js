function assert(message, expr) {
    if (!expr) {
        output(false, message);
        throw new Error(message);
    }
    output(true, message);
}

function output(result, message) {
    var p = document.createElement('p');
    message += result ? ' : SUCCESS' : ' : FAILURE';
    p.style.color = result ? '#0c0' : '#c00';
    p.innerHTML = message;
    document.body.appendChild(p);
}


function testCase(message, tests) {
    // Initialisations 
    var total = 0; // compteur du nombre de test de la suite
    var succeed = 0; // nombre de test réussis
    var h = document.createElement('h3');
    h.innerHTML = message;
    document.body.appendChild(h);
    // Parcours des tests
    for (test in tests) {
        total++;
        try {
            // Passage du test
            tests[test]();
            succeed++;
        } catch (err) {
            // Erreurs non renvoyées pour passer les autres tests
        }
    }
    var p = document.createElement('p');
    p.innerHTML = 'succeeded tests ' + succeed + '/' + total;
    document.body.appendChild(p);
}


testCase('Conversion euro → dollars us', {
    'Test avec 1 euro': function () {
        assert('1€ doit renvoyer 1,3$', convertirEuro(1, 'USD') === 1.3);
    },
    'Test avec 2 euro': function () {
        assert('2€ doit renvoyer 2,6$', convertirEuro(2, 'USD') === 2.6);
    }
})


testCase('Conversion euro → livres anglaises', {
    'Test avec 1 euro': function () {
        assert('1€ doit renvoyer 0,87£', convertirEuro(1, 'GBP') === 0.87);
    },
    'Test avec 2 euro': function () {
        assert('2€ doit renvoyer 1,74£', convertirEuro(2, 'GBP') === 1.74);
    }
})


testCase('Conversion euro → Yen Japonais', {
    'Test avec 1 euro': function () {
        assert('1€ doit renvoyer  124,77¥', convertirEuro(1, 'JPY') === 124.77);
    },
    'Test avec 2 euro': function () {
        assert('2€ doit renvoyer  249.54¥', Math.abs(convertirEuro(2, 'JPY') - 249.54)<0.0001);
    }
})

testCase('Conversion euro → devise non gérée, CRN', {
    'Test devise CRN ': function () {
        var messsage;
        try {
           convertirEuro(1, 'CRN')
        } catch(err) {
           message = err.message;
        }

        assert('La conversion euro → crn doit renvoiyer une erreur', 
        message === 'Monnaie non geree');   
    }
})

