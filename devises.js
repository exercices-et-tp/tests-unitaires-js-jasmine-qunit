function convertirEuro(euro, devise){
    switch(devise){
     case 'USD' :
      return euro * 1.3; 
     case 'GBP' :
      return euro * 0.87;
     case 'JPY' :
      return euro * 124.77;
     default : {
      throw new Error('Monnaie non geree');
     }
    }
   }
