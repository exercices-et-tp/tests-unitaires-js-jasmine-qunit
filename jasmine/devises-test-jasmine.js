
describe('Conversion euro → dollars us (USD)', function() {
    var monnaie = 'USD';
    it("Test avec 1 euro", function() {
      expect( convertirEuro(1, monnaie) ).toEqual(1.3);
    });
    it("Test avec 2 euros", function() {
        expect( convertirEuro(2, monnaie) ).toBeCloseTo(2.6,3);
      });
  });
  
describe('Conversion euro → livres anglaises (GBP)', function() {
    var monnaie = 'GBP';
    it("Test avec 1 euro", function() {
      expect( convertirEuro(1, monnaie) ).toBe(0.87);
    });
    it("Test avec 2 euros", function() {
        expect( convertirEuro(2, monnaie) ).toBe(1.74);
      });
  });
  
describe('Conversion euro → Yen Japonais (JPY)', function() {
    var monnaie = 'JPY';
    it("Test avec 1 euro", function() {
      expect( convertirEuro(1, monnaie) ).toBe(124.77);
    });
    it("Test avec 2 euros", function() {
        expect( convertirEuro(2, monnaie) ).toBeCloseTo(249.54,2);
      });
  });
  
// Dans ce cas il faut une fonction anonyme pour récupérer l'erreur
describe('Conversion euro → devise non gérée, CRN', function() {
    it("Test envoi Erreur", function() {
    expect( function(){ convertirEuro(2, 'CRN')} ).toThrowError(Error, 'Monnaie non geree'); 
  });
});


 describe('Tableau de tests de conversion', function() {
  // Création d'un tableau
  // [0]:devise, [1]:montant €, [2]:résultat attendu, [3]:caractère monnaie 
  var tabAttendusTests  = [
    ['USD', 1, 1.3, '$'],
    ['USD', 2, 2.6, '$'],
    ['GBP', 1, 0.87, '£'],
    ['GBP', 2, 1.74, '£'],
    ['JPY', 1, 124.77, '¥'],
    ['JPY', 2, 249.54, '¥']
  ]
  
  tabAttendusTests.forEach(test => { 
    it(`conversion ${test[1]}€ → ${test[0]} : ${test[2]}`, function() {
      expect( convertirEuro(test[1], test[0]) ).toBeCloseTo(test[2],2);
    });
  });
  
 })
