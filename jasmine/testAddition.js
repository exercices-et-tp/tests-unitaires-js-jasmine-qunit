describe("Série de tests sur l'addition", function() {

    it("ajoute deux nombres", function() {
      expect(2+3).toEqual(5);
    });

    it("ajoute deux autres nombres", function() {
        expect(-2+3).toEqual(-1);
    });
  
});
      