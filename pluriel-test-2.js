function assert(message, expr){
    if(!expr) {
      output(false, message);
      throw new Error(message);
    }
    output(true, message);
   }
   
   function output(result, message){
    var p = document.createElement('p');
    message += result ? ' : SUCCESS' : ' : FAILURE';
    p.style.color = result ? '#0c0' : '#c00';
    p.innerHTML = message;
    document.body.appendChild(p);
   }
   
   
   
   assert("0€ affiche J'ai 0 euro", afficherArgentRestant(0) === "J'ai 0 euro");
   assert("1€ affiche J'ai 1 euro", afficherArgentRestant(1) === "J'ai 1 euro");
   assert("2€ affiche J'ai 2 euros", afficherArgentRestant(2) === "J'ai 2 euros");