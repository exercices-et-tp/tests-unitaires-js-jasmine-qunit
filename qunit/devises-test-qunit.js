
QUnit.module('Conversion euro → dollars us', function() {
  var monnaie = 'USD';

  QUnit.test('Test avec 1 euro', function(assert) {
    assert.equal(convertirEuro(1, monnaie), 1.3, '1€ → 1.3$');
  });

  QUnit.test('Test avec 2 euros', function(assert) {
    assert.equal(convertirEuro(2, monnaie), 2.6, '1€ → 2.6$');
  });

});

// Création d'un tableau
// [0]:devise, [1]:montant €, [2]:résultat attendu, [3]:caractère monnaie 
var tabAttendusTests  = [
  ['USD', 1, 1.3, '$'],
  ['USD', 2, 2.6, '$'],
  ['GBP', 1, 0.87, '£'],
  ['GBP', 2, 1.74, '£'],
  ['JPY', 1, 124.77, '¥'],
  ['JPY', 2, 249.54, '¥']
]

// Doc : https://api.qunitjs.com/QUnit/module/
QUnit.module('Tableau de tests de conversion');

tabAttendusTests.forEach(test => { 
  QUnit.test(`conversion ${test[1]}€ → ${test[0]}`, function(assert) {
    assert.equal(convertirEuro(test[1], test[0]), test[2], `${test[1]}€ → ${test[2]+test[0]}`);
  }); 
});

QUnit.module('Error devises non gérées');
QUnit.test('euro → devise non gérée, CRN', function(assert) {
  assert.throws(function() { convertirEuro(2, 'CRN') }, 'Monnaie non geree');
}); 
