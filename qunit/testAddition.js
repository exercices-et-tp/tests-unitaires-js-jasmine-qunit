QUnit.module('add', function() {
  QUnit.test('Addition de 2 nombres positifs', function(assert) {
    assert.equal(1+1, 2, '1 + 1 = 2');
  });

  QUnit.test('Addition de 2 nombres négatifs', function(assert) {
    assert.equal(-1+ (-1), -2, '-1 + (-1) = -2');
  });

});